const swiper = new Swiper(".mySwiper", {
	// autoHeight: true,
	spaceBetween: 30,
	slidesPerView: 2,
	grabCursor: true,
	loop: true,

	breakpoints: {
		320: {
			slidesPerView: 1,
			spaceBetween: 0,
		},
		565: {
			slidesPerView: 2,
			spaceBetween: 20,
		},
		1600: {
			spaceBetween: 30,
		},
	},
	/*navigation: {
		nextEl: ".swiper-button-next",
		prevEl: ".swiper-button-prev",
	},*/

	/*pagination: {
		el: ".swiper-pagination",
		clickable: true,
	},*/
});

const swiper2 = new Swiper(".mySwiper2", {
	// autoHeight: true,
	spaceBetween: 30,
	slidesPerView: 2,
	grabCursor: true,
	loop: true,
	navigation: {
		/*nextEl: ".swiper-button-next",
		prevEl: ".swiper-button-prev",*/
		nextEl: ".slider-button-next",
		prevEl: ".slider-button-prev",
	},
	breakpoints: {
		320: {
			slidesPerView: 1,
			spaceBetween: 0,
		},
		565: {
			slidesPerView: 2,
			spaceBetween: 20,
		},
		1600: {
			spaceBetween: 30,
		},
	},

	/*pagination: {
		el: ".swiper-pagination",
		clickable: true,
	},*/
});


const row = document.querySelector('#row');
const row2 = document.querySelector('#row2');
const row3 = document.querySelector('#row3');

row.onclick = function () {
	this.classList.toggle('rotate')
	const showSubMenu = document.querySelector('.mobile_nav__sub_list.show-mod')
	if(showSubMenu) {
		showSubMenu.classList.toggle('hidden')
	}
}
row2.onclick = function () {
	this.classList.toggle('rotate')
	const showSubMenu = document.querySelector('.mobile_nav__sub_list.show-mod2')
	if(showSubMenu) {
		showSubMenu.classList.toggle('hidden')
	}
}
row3.onclick = function () {
	this.classList.toggle('rotate')
	const showSubMenu = document.querySelector('.mobile_nav__sub_list.show-mod3')
	if(showSubMenu) {
		showSubMenu.classList.toggle('hidden')
	}
}

function show(num) {
	const showSubMenu = document.querySelector('.mobile_nav__sub_list.show-mod'+num)

	if(showSubMenu) {
		showSubMenu.classList.toggle('hidden')
	}
}

// header menu
document.querySelector('.menu_icon').onclick = function () {
	const mobMenu = document.querySelector('.mobile_nav')

	mobMenu.classList.toggle('mobile_nav_active')
	this.classList.toggle('menu_icon_active')
}

// aside menu
const box = document.querySelectorAll('.nav_sidebar_link');

// let prevActive = localStorage.getItem('activeId');
// if(prevActive) document.getElementById(prevActive).classList.add('nav_sidebar_link_active')

for (let i = 0; i < box.length; i++) {
	box[i].onclick = function() {
		for (let j = 0; j < box.length; j++) {
			box[j].classList.remove('nav_sidebar_link_active');
		}
		this.classList.add('nav_sidebar_link_active');
		// localStorage.setItem('activeId', this.id);
	}
}

// Hide Mobile menu after onclick
const mobileNav = document.querySelector('.mobile_nav');
mobileNav.addEventListener('click', function (e) {
	const target = e.target;
	const link = target.getAttribute('href')
	console.log(link);

	if (link.length > 3) {
		const  menuIcon = document.querySelector('.menu_icon');
		const mobMenu = document.querySelector('.mobile_nav')

		mobMenu.classList.toggle('mobile_nav_active')
		menuIcon.classList.toggle('menu_icon_active')
	}
	
	/*if (this.classList.contains('mobile_nav_active')) {
		this.classList.remove('mobile_nav_active')
	}*/
})


// Mobile active menu
const box2 = document.querySelectorAll('.mobile_nav__sub_link');

let prevActive = sessionStorage.getItem('activeId');
if(prevActive) document.getElementById(prevActive).classList.add('nav_sidebar_link_active')

for (let i = 0; i < box2.length; i++) {
	box2[i].onclick = function() {
		for (let j = 0; j < box2.length; j++) {
			box2[j].classList.remove('nav_sidebar_link_active');
		}
		this.classList.add('nav_sidebar_link_active');
		sessionStorage.setItem('activeId', this.id);
	}
}


window.addEventListener('resize', function(){
 	const submenuElem = document.querySelectorAll('.mobile_nav__sub_link');
	for (const submenuElemKey of submenuElem) {
		if (submenuElemKey.classList.contains('nav_sidebar_link_active')) {
			console.log(submenuElemKey)
			const href = submenuElemKey.getAttribute('href');
			console.log(href)
		  const anchor = href.split('#')[1]
			console.log(anchor)
			// location.hash
			document.querySelector('#' + anchor).scrollIntoView()
		}
	}
});

